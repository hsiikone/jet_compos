
#ifndef AnalyzeData_h
#define AnalyzeData_h

#include "settings.h"

#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <TStopwatch.h>
#include <TH1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TGraph.h>
#include <THStack.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <vector>
#include <iostream>
#include <string>
#include <cassert>
#include <set>
#include <sstream>
#include <regex>

#include "JetMETObjects/interface/FactorizedJetCorrector.h"
#include "JetMETObjects/interface/JetCorrectorParameters.h"
#include "JetMETObjects/interface/JetCorrectionUncertainty.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::regex;

const int kMaxPFJets = 200;
const vector<double> ptLimits =
//{1, 5, 6, 8, 10, 12, 15, 18, 21, 24, 28, 32, 37, 43, 49,
    {56, 64, 74, 84, 97, 114, 133, 153, 174, 196, 220, 245, 272,
    300, 330, 362, 395, 430, 468, 507, 548, 592, 638, 686, 737,
    790, 846, 905, 967, 1032, 1101, 1172, 1248, 1327, 1410, 1497,
    1588, 1684, 1784, 1890, 2000,
    2116, 2238, 2366, 2500, 2640, 2787, 2941, 3103, 3273, 3450, 3637, 3832, 4037};

// eta binning based on the detector structure (at least mostly ATM..)
// const double etabins[] = { -5.205, -4.903, -4.73, -4.552, -4.377, -4.027,
//     -3.853, -3.677, -3.503, -3.327, -3.152, -3.000, -2.650, -2.322, -2.043,
//     -1.830, -1.653, -1.479, -1.305, -1.131, -0.957, -0.783, -0.522, -0.261,
//     0, 0.261, 0.522, 0.783, 0.957, 1.131, 1.305, 1.479, 1.653, 1.830, 2.043,
//     2.322, 2.650, 3.000, 3.152, 3.327, 3.503, 3.677, 3.853, 4.027, 4.377,
//     4.552, 4.730, 4.903, 5.205 }; // CMS JINST p. 145 Table 5.8 & Figure 5.29

struct Profiles
{
    TProfile2D *chfpu;
    TProfile2D *chf;
    TProfile2D *phf;
    TProfile2D *nhf;
    TProfile2D *elfMuf;
    TProfile2D *hfHf;
    TProfile2D *hfPhf;
    TH2D *ptBins;
    TH2D *ptBinsScaled;

    Profiles(double etaLim=5.0, unsigned etaAmnt=100) {
        chfpu = new TProfile2D("chfpu2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        chf = new TProfile2D("chf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        phf = new TProfile2D("phf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        nhf = new TProfile2D("nhf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        elfMuf = new TProfile2D("elfmuf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        hfHf = new TProfile2D("hfhf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        hfPhf = new TProfile2D("hfphf2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        ptBins = new TH2D("ptbins2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        ptBinsScaled = new TH2D("ptbinsscaled2D","",ptLimits.size()-1,&ptLimits[0],etaAmnt,-etaLim,etaLim);
        ptBins->SetMinimum(0.0000000001);
    }
};


/* Tags for an event */
struct EventID
{
    Long64_t run;
    Long64_t lumi;
    Long64_t event;
    EventID(Long64_t r, Long64_t l, Long64_t e): run(r), lumi(l), event(e) {};

    bool operator<(const EventID& rhs) const {
        return rhs.run < run || (rhs.run == run && (rhs.lumi < lumi ||
        (rhs.lumi == lumi && rhs.event < event)));
    }

    bool operator==(const EventID& rhs) const {
        return rhs.run == run && (rhs.lumi == lumi && rhs.event == event);
    }
};





class AnalyzeData {
public :
    TTree          *mChain;   //!pointer to the analyzed TTree or TChain
    Int_t           mCurrent; //!current Tree number in a TChain
    Long64_t        mEvents;
    Int_t           mIsMC;
    string          mTag;
    vector<vector<unsigned> > mTriggerIDs;

    //QCDEvent        *events;
    Int_t           mRun;
    Int_t           mEvent;
    Int_t           mLumi;
    Int_t           mNVtxGood;
    Float_t         mTrPu;
    Float_t         mPthat;
    Float_t         mPFRho;
    Float_t         mPFMetEt;
    Float_t         mPFMetSumEt;
    Float_t         mWeight;
    vector<int>     mTriggerDecision;
    vector<int>     mL1;
    vector<int>     mHLT;
    Int_t           mPFJets;
    Double_t        mPx[kMaxPFJets];   //[mPFJets]
    Double_t        mPy[kMaxPFJets];   //[mPFJets]
    Double_t        mPz[kMaxPFJets];   //[mPFJets]
    Double_t        mE[kMaxPFJets];   //[mPFJets]
    Double_t        mPxGen[kMaxPFJets];   //[mPFJets]
    Double_t        mPyGen[kMaxPFJets];   //[mPFJets]
    Double_t        mPzGen[kMaxPFJets];   //[mPFJets]
    Double_t        mEGen[kMaxPFJets];   //[mPFJets]
    Float_t         mCorr[kMaxPFJets];   //[mPFJets]
    Float_t         mArea[kMaxPFJets];   //[mPFJets]
    Bool_t          mLooseID[kMaxPFJets];   //[mPFJets]
    Bool_t          mTightID[kMaxPFJets];   //[mPFJets]
    Float_t         mChf[kMaxPFJets];   //[mPFJets]
    Float_t         mNhf[kMaxPFJets];   //[mPFJets]
    Float_t         mPhf[kMaxPFJets];   //[mPFJets]
    Float_t         mElf[kMaxPFJets];   //[mPFJets]
    Float_t         mMuf[kMaxPFJets];   //[mPFJets]
    Float_t         mHf_hf[kMaxPFJets];   //[mPFJets]
    Float_t         mHf_phf[kMaxPFJets];   //[mPFJets]
    Float_t         mBetaStar[kMaxPFJets];   //[mPFJets]

    AnalyzeData(TTree * = 0, Long64_t = 0, Int_t = 0, string = "");
    virtual ~AnalyzeData();
    virtual Int_t    GetEntry(Long64_t);
    virtual Long64_t LoadTree(Long64_t);
    virtual void     Init(TTree *);
    virtual void     Loop(string);
    virtual void     Show(Long64_t = -1);

private:
    FactorizedJetCorrector *EnergyCorrSetup(vector<JetCorrectorParameters> *corParams, Int_t isMC);
    string GivePath(Int_t correction, Int_t isMC);
    void GetTriggers();
    int TriggerType(double pt);
};

#endif

#ifdef AnalyzeData_cxx

AnalyzeData::AnalyzeData(TTree *tree, Long64_t events, Int_t mc, string tag) : mChain(0)
{
    mEvents=events;
    mIsMC=mc;
    mTag = tag;
    assert(_trigger_limits.size()==_trigger_names.size()+1); /* For a functional indexing these have to match */
    Init(tree);
}

AnalyzeData::~AnalyzeData()
{
    if (!mChain) return;
    delete mChain->GetCurrentFile();
}

Int_t AnalyzeData::GetEntry(Long64_t entry)
{
    if (!mChain) return 0;
    return mChain->GetEntry(entry);
}

Long64_t AnalyzeData::LoadTree(Long64_t entry)
{
    if (!mChain) return -5;
    Long64_t centry = mChain->LoadTree(entry);
    if (centry < 0)
        return centry;
    if (mChain->GetTreeNumber() != mCurrent) {
        mCurrent = mChain->GetTreeNumber();
        if (!mIsMC)
            GetTriggers();
    }
    return centry;
}

void AnalyzeData::Init(TTree *tree)
{
    if (!tree) return;
    mChain = tree;
    mCurrent = -1;
    mChain->SetMakeClass(1);

    mChain->SetBranchAddress("EvtHdr_.mRun", &mRun);
    mChain->SetBranchAddress("EvtHdr_.mEvent", &mEvent);
    mChain->SetBranchAddress("EvtHdr_.mLumi", &mLumi);
    mChain->SetBranchAddress("EvtHdr_.mNVtxGood", &mNVtxGood);
    mChain->SetBranchAddress("EvtHdr_.mTrPu", &mTrPu);
    mChain->SetBranchAddress("EvtHdr_.mPthat", &mPthat);
    mChain->SetBranchAddress("EvtHdr_.mPFRho", &mPFRho);
    mChain->SetBranchAddress("EvtHdr_.mWeight", &mWeight);
    mChain->SetBranchAddress("PFMet_.et_", &mPFMetEt);
    mChain->SetBranchAddress("PFMet_.sumEt_", &mPFMetSumEt);
    mChain->SetBranchAddress("TriggerDecision_", &mTriggerDecision);
    mChain->SetBranchAddress("L1Prescale_", &mL1);
    mChain->SetBranchAddress("HLTPrescale_", &mHLT);
    mChain->SetBranchAddress("PFJets_", &mPFJets);
    mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fX", mPx);
    mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fY", mPy);
    mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fZ", mPz);
    mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fT", mE);
    mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fX", mPxGen);
    mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fY", mPyGen);
    mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fZ", mPzGen);
    mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fT", mEGen);
    mChain->SetBranchAddress("PFJets_.cor_", mCorr);
    mChain->SetBranchAddress("PFJets_.area_", mArea);
    mChain->SetBranchAddress("PFJets_.looseID_", mLooseID);
    mChain->SetBranchAddress("PFJets_.tightID_", mTightID);
    mChain->SetBranchAddress("PFJets_.chf_", mChf);
    mChain->SetBranchAddress("PFJets_.nhf_", mNhf);
    mChain->SetBranchAddress("PFJets_.nemf_", mPhf);
    mChain->SetBranchAddress("PFJets_.cemf_", mElf);
    mChain->SetBranchAddress("PFJets_.muf_", mMuf);
    mChain->SetBranchAddress("PFJets_.hf_hf_", mHf_hf);
    mChain->SetBranchAddress("PFJets_.hf_phf_", mHf_phf);
    mChain->SetBranchAddress("PFJets_.betaStar_", mBetaStar);

    /* First disable all branches and then enable the branches in use */
    mChain->SetBranchStatus("*",0);
    mChain->SetBranchStatus("EvtHdr_.mRun", 1);
    mChain->SetBranchStatus("EvtHdr_.mEvent", 1);
    mChain->SetBranchStatus("EvtHdr_.mLumi", 1);
    mChain->SetBranchStatus("EvtHdr_.mNVtxGood", 1);
    mChain->SetBranchStatus("EvtHdr_.mTrPu", 1);
    mChain->SetBranchStatus("EvtHdr_.mPthat", 1);
    mChain->SetBranchStatus("EvtHdr_.mPFRho", 1);
    mChain->SetBranchStatus("EvtHdr_.mWeight", 1);
    mChain->SetBranchStatus("PFMet_.et_", 1);
    mChain->SetBranchStatus("PFMet_.sumEt_", 1);
    mChain->SetBranchStatus("TriggerDecision_", 1);
    mChain->SetBranchStatus("L1Prescale_", 1);
    mChain->SetBranchStatus("HLTPrescale_", 1);
    mChain->SetBranchStatus("PFJets_", 1);
    mChain->SetBranchStatus("PFJets_.P4_*",1);
    mChain->SetBranchStatus("PFJets_.genP4_*", 1);
    mChain->SetBranchStatus("PFJets_.cor_", 1);
    mChain->SetBranchStatus("PFJets_.area_", 1);
    mChain->SetBranchStatus("PFJets_.looseID_", 1);
    mChain->SetBranchStatus("PFJets_.tightID_", 1);
    mChain->SetBranchStatus("PFJets_.chf_", 1);
    mChain->SetBranchStatus("PFJets_.nhf_", 1);
    mChain->SetBranchStatus("PFJets_.nemf_", 1);
    mChain->SetBranchStatus("PFJets_.cemf_", 1);
    mChain->SetBranchStatus("PFJets_.muf_", 1);
    mChain->SetBranchStatus("PFJets_.hf_hf_", 1);
    mChain->SetBranchStatus("PFJets_.hf_phf_", 1);
    mChain->SetBranchStatus("PFJets_.betaStar_", 1);
}

void AnalyzeData::Show(Long64_t entry)
{
    if (!mChain) return;
    mChain->Show(entry);
}

#endif // #ifdef AnalyzeData_cxx
