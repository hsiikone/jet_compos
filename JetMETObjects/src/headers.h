#include "JetMETObjects/interface/JetCorrectorParameters.h"
#include "JetMETObjects/interface/JetCorrectionUncertainty.h"
#include "JetMETObjects/interface/FactorizedJetCorrector.h"
#include "JetMETObjects/interface/SimpleJetCorrectionUncertainty.h"
#include "JetMETObjects/interface/SimpleJetCorrector.h"
#include "JetMETObjects/interface/JetCorrectorParameters.h"
#include "JetMETObjects/interface/FFTJetCorrectorParameters.h"
#include "JetMETObjects/interface/QGLikelihoodObject.h"
#include "JetMETObjects/interface/METCorrectorParameters.h"
#include "JetMETObjects/interface/JetResolutionObject.h"

#include <vector>
