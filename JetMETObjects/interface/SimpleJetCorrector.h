#ifndef SimpleJetCorrector_h
#define SimpleJetCorrector_h

#include <string>
#include <vector>

#include "JetMETObjects/interface/JetCorrectorParameters.h"
#include "Utils/interface/FormulaEvaluator.h"

namespace formula {
  struct ArrayAdaptor {
  ArrayAdaptor(double const* iStart, size_t iSize):
    m_start(iStart), m_size(iSize) {}
    size_t size() const { return m_size; }
    bool empty() const {return m_size == 0; }
    double const* start() const { return m_start; }
  private:
    double const* m_start;
    size_t m_size;
  };
  inline double const* startingAddress(ArrayAdaptor const& iV) {
    if(iV.empty()) {
      return nullptr;
    }
    return iV.start();
  }

  class EvaluatorBase;
  inline double const* startingAddress(std::vector<double> const& iV) {
    if(iV.empty()) {
      return nullptr;
    }
    return &iV[0];
  }

  template<size_t t>
    inline double const* startingAddress(std::array<double,t> const& iV) {
    if(iV.empty()) {
      return nullptr;
    }
    return &iV[0];
  }
}

class JetCorrectorParameters;

class SimpleJetCorrector 
{
 public:
  //-------- Constructors --------------
  SimpleJetCorrector(const std::string& fDataFile, const std::string& fOption = "");
  SimpleJetCorrector(const JetCorrectorParameters& fParameters);
  //-------- Member functions -----------
  void   setInterpolation(bool fInterpolation) {mDoInterpolation = fInterpolation;}
  float  correction(const std::vector<float>& fX,const std::vector<float>& fY) const;  
  const  JetCorrectorParameters& parameters() const {return mParameters;} 

 private:
  //-------- Member functions -----------
  SimpleJetCorrector(const SimpleJetCorrector&);
  SimpleJetCorrector& operator= (const SimpleJetCorrector&);
  float    invert(const double *args, const double *params) const;
  float    correctionBin(unsigned fBin,const std::vector<float>& fY) const;
  unsigned findInvertVar();
  void     setFuncParameters();
  //-------- Member variables -----------
  JetCorrectorParameters  mParameters;
  reco::FormulaEvaluator  mFunc;
  unsigned                mInvertVar; 
  bool                    mDoInterpolation;
};

#endif


