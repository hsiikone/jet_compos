#include "Utils/src/ExpressionSetter.h"
#include "Utils/src/AndCombiner.h"
#include "Utils/src/OrCombiner.h"
#include "Utils/src/NotCombiner.h"
#include "Utils/interface/Exception.h"

using namespace reco::parser;

void ExpressionSetter::operator()( const char *begin, const char * ) const {
  if ( exprStack_.size() == 0 ) 
    throw Exception( begin )
      << "Grammar error: When trying parse an expression, expression stack is empty! Please contact a developer.";
  expr_ = exprStack_.back();
}
