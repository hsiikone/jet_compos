#ifndef SETTINGS_H
#define SETTINGS_H

#include <iostream>
#include <TROOT.h>
#include <TSystem.h>
#include <TChain.h>
#include <string>
#include <vector>
#include <map>
#include <regex>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::map;
using std::regex;
using std::pair;

#include "JetMETObjects/interface/FactorizedJetCorrector.h"
#include "JetMETObjects/interface/JetCorrectorParameters.h"
#include "JetMETObjects/interface/JetCorrectionUncertainty.h"

string _pu_dt_tag = "RunG";

string _jec_file = "JetMETObjects/data/Spring16_25nsV6_";

/* Luminosities for each trigger for DT (includes PFJet500) */
vector<double> _trigger_lumi = {
    227834.792,
    629822.985,
    2468355.373,
    21428981.332,
    94676061.988,
    510573038.617,
    1522019655.941,
    4451438401.059,
    29522909900.17,
    29522909900.17
};

vector<int> _trigger_limits = {
    0,
    74,
    114,
    174,
    245,
    330,
    395,
    468,
    548,
    6500
};

vector< vector<string> > _trigger_names = {
    {"PFJet40"},
    {"PFJet60"},
    {"PFJet80"},
    {"PFJet140"},
    {"PFJet200"},
    {"PFJet260"},
    {"PFJet320"},
    {"PFJet400"},
    {"PFJet450","PFJet500"}
};

vector<string> _trigger_tags = {
    "40",
    "60",
    "80",
    "140",
    "200",
    "260",
    "320",
    "400",
    "450"
};


// Pileup profiles:
map<string,string> _pu_profs = {
    {"MC_P8_1","pileup/pileup_PY_80X.root"}
    ,
    {"MC_HP_2","pileup/pileup_HPP_80X.root"}
    ,
    {"RunB","pileup/RunB/pileup_DT.root"}
    ,
    {"RunC","pileup/RunC/pileup_DT.root"}
    ,
    {"RunD","pileup/RunD/pileup_DT.root"}
    ,
    {"RunE","pileup/RunE/pileup_DT.root"}
    ,
    {"RunF","pileup/RunF/pileup_DT.root"}
    ,
    {"RunG","pileup/RunG/pileup_DT.root"}
    ,
    {"RunBCD","pileup/RunBCD/pileup_DT.root"}
};

string _path = "root://eoscms.cern.ch//eos/cms/store/group/phys_smp/Multijet/13TeV/";

map< string, vector<string> > _runs = {
    {"RunB",{
     "Data/2016/Ntuples-Data-2016-RunB-part1.root",
     "Data/2016/Ntuples-Data-2016-RunB-part2.root",
     "Data/2016/Ntuples-Data-2016-RunB-part3.root"}}
    ,
    {"RunC",{
     "Data/2016/Ntuples-Data-2016-RunC-v2-part1.root",
     "Data/2016/Ntuples-Data-2016-RunC-v2-part2.root",
     "Data/2016/Ntuples-Data-2016-RunC-v2-part3.root"}}
    ,
    {"RunD",{
     "Data/2016/Ntuples-Data-2016-RunD-part1.root",
     "Data/2016/Ntuples-Data-2016-RunD-part2.root"}}
    ,
    {"RunE",{
     "Data/2016/Ntuples-Data-2016-RunE-part1.root",
     "Data/2016/Ntuples-Data-2016-RunE-part2.root",
     "Data/2016/Ntuples-Data-2016-RunE-part3.root"}}
    ,
    {"RunF",{
     "Data/2016/Ntuples-Data-2016RunF-PromptReco-80Xpart1.root",
     "Data/2016/Ntuples-Data-2016RunF-PromptReco-80Xpart2.root"}}
    ,
    {"RunG",{
     "Data/2016/Ntuples-Data-2016RunG-PromptReco-80Xpart1.root",
     "Data/2016/Ntuples-Data-2016RunG-PromptReco-80Xpart2.root",
     "Data/2016/Ntuples-Data-2016RunG-PromptReco-80Xpart3.root",
     "Data/2016/Ntuples-Data-2016RunG-PromptReco-80Xpart4.root"}}
    ,
    {"MC_P8_1",{
     "MC/Ntuples-MC-Pythia8-Flat15to7000-25ns-CUETM1-13TeV.root"}}
    ,
    {"MC_HP_1",{
     "MC/QCD_Pt-15to7000_TuneCUETHS1_Flat_13TeV_herwigpp_80X.root"}}
    ,
    {"MC_HP_2",{
     "MC/QCD_Pt-15to7000_TuneCUETHS1_Flat_13TeV_herwigpp_80X_v2.root"}}
};

#endif // SETTINGS_H
